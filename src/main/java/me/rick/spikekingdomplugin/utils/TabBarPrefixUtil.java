package me.rick.spikekingdomplugin.utils;

import org.bukkit.entity.Player;

/**
 * Deze class zorgt er voor dat als je op tab drukt je de prefix van een speler voor ze naam ziet.
 * @author RickVeerman
 */
public class TabBarPrefixUtil {

    /**
     * Checks if prefix is not null and adds the prefix before the name.
     * @param player | Player
     * @param prefix | Prefix
     */
    public static void setTabBarPrefix(Player player, String prefix) {
        if (prefix != null) {
            player.setPlayerListName(prefix + player.getName());
        }
    }
}

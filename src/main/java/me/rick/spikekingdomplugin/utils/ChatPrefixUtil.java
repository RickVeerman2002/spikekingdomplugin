package me.rick.spikekingdomplugin.utils;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * This class handles the chatprefix when setting a new rank or kingdom and when a player is joining.
 * @author RickVeerman
 */
public class ChatPrefixUtil {
    /**
     * Checks if a player has a rank and gives a rank value to the "onChatPrefix()" function
     * @param config | File Configuration
     * @param player | Player
     */
    public static void onSetCommand(FileConfiguration config, Player player) {
        String rank;
        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");

        } else {
            rank = null;
        }
        onChatPrefix(config, player, rank);
    }

    /**
     * Gives the player the member rank if they have no rank, checks if a player has a rank and gives a rank value to the "onChatPrefix()" function
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param player | Player
     */
    public static void onJoinListener(SpikeKingdomPlugin plugin, FileConfiguration config, Player player) {
        String rank;
        if(!config.contains("players." + player.getName() + ".rank")){
            rank = "Member";
            config.set("players." + player.getName() + ".rank", rank);
            plugin.saveConfig();
        } else {
            rank = config.getString("players." + player.getName() + ".rank");
        }

        onChatPrefix(config, player, rank);
    }

    /**
     * Sets the rank and kingdom prefix if the player has one
     * @param config | File Configuration
     * @param player | Player
     * @param rank | Rank Name
     */
    public static void onChatPrefix(FileConfiguration config, Player player, String rank) {

        String kingdom;
        String kingdomRank;
        String rankPrefix;
        String kingdomPrefix;
        String kingdomRankPrefix;

        if(config.contains("players." + player.getName() + ".kingdom")) {
            kingdom = config.getString("players." + player.getName() + ".kingdom");
        } else {
            kingdom = null;
        }

        //If the player that sends a message, and he has a kingdom rank. Kingdom Rank gets a value
        if(config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }

        rankPrefix = config.getString("ranks." + rank + ".prefix");
        kingdomPrefix = config.getString("kingdoms." + kingdom + ".prefix");
        kingdomRankPrefix = config.getString("kingdomRanks." + kingdomRank + ".prefix");

        //If rankPrefix is not null, kingdomPrefix is null and kingdomRankPrefix is null the player gets a rank prefix
        if (rankPrefix != null && kingdomPrefix == null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + rankPrefix + "&7]&f " );
            TabBarPrefixUtil.setTabBarPrefix(player, format);
        }
        //If kingdomPrefix & rankPrefix are not null the player gets a rank and kingdom prefix
        if (kingdomPrefix != null && rankPrefix != null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + rankPrefix + "&7][" + kingdomPrefix + "&7]&f ");
            TabBarPrefixUtil.setTabBarPrefix(player, format);
        }

        //If kingdomPrefix & rankPrefix are not null the player gets a rank and kingdom prefix
        if (kingdomPrefix != null && rankPrefix != null && kingdomRankPrefix != null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + rankPrefix + "&7][" + kingdomPrefix + "&7][" + kingdomRankPrefix + "&7]&f " );
            TabBarPrefixUtil.setTabBarPrefix(player, format);
        }

        //If rankPrefix is null and kingdomPrefix is not null the player gets a kingdom prefix
        if (rankPrefix == null && kingdomPrefix != null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + kingdomPrefix + "&7]&f ");
            TabBarPrefixUtil.setTabBarPrefix(player, format);
        }

        //If rankPrefix is null and kingdomPrefix is not null the player gets a kingdom prefix
        if (rankPrefix == null && kingdomPrefix != null && kingdomRankPrefix != null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + kingdomPrefix + "&7][" + kingdomRankPrefix + "&7]&f ");
            TabBarPrefixUtil.setTabBarPrefix(player, format);
        }


    }

}

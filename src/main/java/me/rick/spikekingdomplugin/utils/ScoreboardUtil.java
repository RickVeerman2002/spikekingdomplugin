package me.rick.spikekingdomplugin.utils;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

/**
 * This class creates a scoreboard with nice information.
 * @author RickVeerman
 */
public class ScoreboardUtil {

    /**
     * This function creates a scoreboard. It checks if the player has a rank or is part of a kingdom and checks of the
     * rank or kingdom a prefix contains. And if the player has that that will be added to the scoreboard
     * @param player | Player
     * @param plugin | SpikeKingdomPlugin
     */
    public static void createScoreboard(Player player, SpikeKingdomPlugin plugin) {
        FileConfiguration config = plugin.getConfig();

        String rank;
        String kingdom;
        String kingdomRank;
        String rankPrefix;
        String kingdomPrefix;
        String kingdomRankPrefix;


        if(!config.contains("players." + player.getName() + ".rank")){
            rank = "Member";
            config.set("players." + player.getName() + ".rank", rank);
            plugin.saveConfig();
        } else {
            rank = config.getString("players." + player.getName() + ".rank");
        }

        if(config.contains("players." + player.getName() + ".kingdom")) {
            kingdom = config.getString("players." + player.getName() + ".kingdom");
        } else {
            kingdom = null;
        }

        if(config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }

        rankPrefix = config.getString("ranks." + rank + ".prefix");
        kingdomPrefix = config.getString("kingdoms." + kingdom + ".prefix");
        kingdomRankPrefix = config.getString("kingdomRanks." + kingdomRank + ".prefix");

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("SpikeKingdom", "dummy");
        objective.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&6&lSpikeKingdom"));
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        if (rankPrefix != null) {
            objective.getScore("").setScore(5);

            objective.getScore(ChatColor.translateAlternateColorCodes('&', "&7Rank: &f" + rankPrefix)).setScore(4);
        }
        if (kingdomPrefix != null) {
            objective.getScore(" ").setScore(3);
            objective.getScore(ChatColor.translateAlternateColorCodes('&', "&7Kingdom: &f" + kingdomPrefix)).setScore(2);
        }

        if (kingdomRankPrefix != null) {
            objective.getScore("  ").setScore(1);
            objective.getScore(ChatColor.translateAlternateColorCodes('&', "&7Kingdom Rank: &f" + kingdomRankPrefix)).setScore(0);
        }

        player.setScoreboard(scoreboard);
    }
}



package me.rick.spikekingdomplugin.utils;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * This class has all function that has to do with adding permissions to players
 * @author RickVeerman
 */
public class AddPermissionUtil {

    /**
     * Adds permissions to one player
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param player | Player
     */
    public static void addsPermissionsToOnePlayer(SpikeKingdomPlugin plugin, FileConfiguration config, Player player) {
        String rank;
        String kingdomRank;
        List<String> allPermissions = new ArrayList<>();
        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");
        } else {
            rank = null;
        }

        if (config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }

        List<String> rankPermissions = config.getStringList("ranks." + rank + ".permissions");
        List<String> kingdomRankPermissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");
        allPermissions.addAll(rankPermissions);
        allPermissions.addAll(kingdomRankPermissions);
        for (String permission : allPermissions) {
            player.addAttachment(plugin, permission, true);
        }
    }

    /**
     * Adds a permission to one player
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param player | Player
     * @param permission | Permission
     */
    public static void addsPermissionToOnePlayer(SpikeKingdomPlugin plugin, FileConfiguration config, Player player, String permission) {
        String rank;
        String kingdomRank;

        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");
        } else {
            rank = null;
        }

        if (config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }


        if (config.getStringList("ranks." + rank + ".permissions").contains(permission)) {
            player.addAttachment(plugin, permission, true);

        }

        if (config.getStringList("kingdomRanks." + kingdomRank + ".permissions").contains(permission)) {
            player.addAttachment(plugin, permission, true);

        }
    }

    /**
     * Adds permissions to all players
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     */
    public static void addsPermissionsToAllPlayers(SpikeKingdomPlugin plugin, FileConfiguration config) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String rank;
            String kingdomRank;
            List<String> allPermissions = new ArrayList<>();
            if (config.contains("players." + player.getName() + ".rank")) {
                rank = config.getString("players." + player.getName() + ".rank");
            } else {
                rank = null;
            }

            if (config.contains("players." + player.getName() + ".kingdomRank")) {
                kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
            } else {
                kingdomRank = null;
            }

            List<String> rankPermissions = config.getStringList("ranks." + rank + ".permissions");
            List<String> kingdomRankPermissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");
            allPermissions.addAll(rankPermissions);
            allPermissions.addAll(kingdomRankPermissions);
            for (String permission : allPermissions) {
                player.addAttachment(plugin, permission, true);
            }
        }
    }

    /**
     * Adds a permission to all players
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param permission | Permission
     */
    public static void addsPermissionToAllPlayer(SpikeKingdomPlugin plugin, FileConfiguration config, String permission) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String rank;
            String kingdomRank;
            if (config.contains("players." + player.getName() + ".rank")) {
                rank = config.getString("players." + player.getName() + ".rank");
            } else {
                rank = null;
            }

            if (config.contains("players." + player.getName() + ".kingdomRank")) {
                kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
            } else {
                kingdomRank = null;
            }


            if (config.getStringList("ranks." + rank + ".permissions").contains(permission)) {
                player.addAttachment(plugin, permission, true);

            }

            if (config.getStringList("kingdomRanks." + kingdomRank + ".permissions").contains(permission)) {
                player.addAttachment(plugin, permission, true);

            }
        }
    }
}

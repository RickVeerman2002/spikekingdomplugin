package me.rick.spikekingdomplugin.utils;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * This class has all function that has to do with removing permissions to players
 * @author RickVeerman
 */
public class RemovePermissionUtil {

    /**
     * Removes permissions to one player
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param player | Player
     */
    public static void removesPermissionsFromOnePlayer(SpikeKingdomPlugin plugin, FileConfiguration config, Player player) {
        String rank;
        String kingdomRank;
        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");
        } else {
            rank = null;
        }

        if (config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }


        List<String> rankPermissions = config.getStringList("ranks." + rank + ".permissions");
        for (String permission : rankPermissions) {
            player.addAttachment(plugin, permission, false);
        }

        List<String> kingdomRankPermissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");
        for (String permission : kingdomRankPermissions) {
            player.addAttachment(plugin, permission, true);
        }

    }

    /**
     * Removes permissions to one player
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param player | Player
     */
    public static void removesKingdomRankPermissionsFromOnePlayer(SpikeKingdomPlugin plugin, FileConfiguration config, Player player) {
        String rank;
        String kingdomRank;

        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");
        } else {
            rank = null;
        }

        if (config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }

        List<String> kingdomRankPermissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");
        for (String permission : kingdomRankPermissions) {
            player.addAttachment(plugin, permission, false);
        }

        List<String> rankPermissions = config.getStringList("ranks." + rank + ".permissions");
        for (String permission : rankPermissions) {
            player.addAttachment(plugin, permission, true);
        }
    }

    /**
     * Removes a permission to one player
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param player | Player
     * @param permission | Permission
     */
    public static void removesPermissionFromOnePlayer(SpikeKingdomPlugin plugin, FileConfiguration config, Player player, String permission) {
        String rank;
        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");
        } else {
            rank = null;
        }

        if (config.getStringList("ranks." + rank + ".permissions").contains(permission)) {
            player.addAttachment(plugin, permission, false);

        }
    }

    /**
     * Removes permissions to all players
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     */
    public static void removesPermissionsFromAllPlayers(SpikeKingdomPlugin plugin, FileConfiguration config) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String rank;
            String kingdomRank;
            if (config.contains("players." + player.getName() + ".rank")) {
                rank = config.getString("players." + player.getName() + ".rank");
            } else {
                rank = null;
            }

            if (config.contains("players." + player.getName() + ".kingdomRank")) {
                kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
            } else {
                kingdomRank = null;
            }

            List<String> rankPermissions = config.getStringList("ranks." + rank + ".permissions");
            for (String permission : rankPermissions) {
                player.addAttachment(plugin, permission, false);
            }

            List<String> kingdomRankPermissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");
            for (String permission : kingdomRankPermissions) {
                player.addAttachment(plugin, permission, true);
            }
        }
    }

    /**
     * Removes permissions to all players
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     */
    public static void removesKingdomRankPermissionsFromAllPlayers(SpikeKingdomPlugin plugin, FileConfiguration config) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String rank;
            String kingdomRank;
            if (config.contains("players." + player.getName() + ".rank")) {
                rank = config.getString("players." + player.getName() + ".rank");
            } else {
                rank = null;
            }

            if (config.contains("players." + player.getName() + ".kingdomRank")) {
                kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
            } else {
                kingdomRank = null;
            }

            List<String> kingdomRankPermissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");
            for (String permission : kingdomRankPermissions) {
                player.addAttachment(plugin, permission, false);
            }

            List<String> rankPermissions = config.getStringList("ranks." + rank + ".permissions");
            for (String permission : rankPermissions) {
                player.addAttachment(plugin, permission, true);
            }


        }
    }

    /**
     * Removes a permission to all players
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param permission | Permission
     */
    public static void removesPermissionFromAllPlayer(SpikeKingdomPlugin plugin, FileConfiguration config, String permission) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String rank;
            if (config.contains("players." + player.getName() + ".rank")) {
                rank = config.getString("players." + player.getName() + ".rank");
            } else {
                rank = null;
            }

            if (config.getStringList("ranks." + rank + ".permissions").contains(permission)) {
                player.addAttachment(plugin, permission, false);

            }
        }
    }

    /**
     * Removes a permission to all players
     * @param plugin | SpikeKingdomPlugin
     * @param config | File Configuration
     * @param permission | Permission
     */
    public static void removesKingdomRankPermissionFromAllPlayer(SpikeKingdomPlugin plugin, FileConfiguration config, String permission) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String rank;
            String kingdomRank;

            if (config.contains("players." + player.getName() + ".rank")) {
                rank = config.getString("players." + player.getName() + ".rank");
            } else {
                rank = null;
            }

            if (config.contains("players." + player.getName() + ".kingdomRank")) {
                kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
            } else {
                kingdomRank = null;
            }

            if (config.getStringList("kingdomRanks." + kingdomRank + ".permissions").contains(permission)) {
                player.addAttachment(plugin, permission, false);

            }

            if (config.getStringList("ranks." + rank + ".permissions").contains(permission)) {
                player.addAttachment(plugin, permission, true);

            }


        }
    }


}

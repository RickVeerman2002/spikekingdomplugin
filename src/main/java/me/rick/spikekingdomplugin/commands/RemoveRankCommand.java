package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesPermissionsFromAllPlayers;

/**
 * This is the class of the command "/removerank". It removes a rank.
 * @author RickVeerman
 */
public class RemoveRankCommand implements CommandExecutor {
    private final SpikeKingdomPlugin plugin;

    /**
     * RemoveRank Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public RemoveRankCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "This command can only be run by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 1 arguments it sends an error message.
        if (args.length < 1) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /removerank <rank>");
            return true;
        }
        String rankToRemove = args[0];
        FileConfiguration config = plugin.getConfig();
        //If the config doesn't contain the given rank it sends an error message.
        if (!config.contains("ranks." + rankToRemove)) {
            player.sendMessage(ChatColor.DARK_RED + "That rank does not exist.");
            return true;
        }

        removesPermissionsFromAllPlayers(plugin, config);

        removesRank(config, rankToRemove);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully removed rank: " + rankToRemove);
        Bukkit.reload();
        return true;
    }

    /**
     * Removes the rank from the config and saves it.
     * @param config | File Configuration
     * @param rankToRemove | First Argument of the Command | Rank Name
     */
    private void removesRank(FileConfiguration config, String rankToRemove) {
        for (String player : config.getConfigurationSection("players").getKeys(false)) {
            if (config.getString("players." + player + ".rank").equals(rankToRemove)) {
                config.set("players." + player + ".rank", null);
            }
        }
        config.set("ranks." + rankToRemove, null);
        plugin.saveConfig();
    }
}


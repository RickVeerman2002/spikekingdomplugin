package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * This is the class of the command "/removekingdom". It removes kingdom rank.
 * @author RickVeerman
 */
public class RemoveKingdomCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * RemoveKingdom Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public RemoveKingdomCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "This command can only be run by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 1 arguments it sends an error message.
        if (args.length != 1) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /removekingdom <kingdom>");
            return true;
        }

        String kingdomToRemove = args[0];
        FileConfiguration config = plugin.getConfig();
        //If the config doesn't contain the given kingdom it sends an error message.
        if (!config.contains("kingdoms." + kingdomToRemove)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom does not exist.");
            return true;
        }

        removesKingdom(config, kingdomToRemove);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully removed kingdom: " + kingdomToRemove);
        return true;
    }

    /**
     * Removes the kingdom from the config and saves it.
     * @param config | File Configuration
     * @param kingdomToRemove | First Argument of the Command | Kingdom Name
     */
    private void removesKingdom(FileConfiguration config, String kingdomToRemove) {
        for (String player : config.getConfigurationSection("players").getKeys(false)) {
            if (config.getString("players." + player + ".kingdom").equals(kingdomToRemove)) {
                config.set("players." + player + ".kingdom", null);
            }
        }
        config.set("kingdoms." + kingdomToRemove, null);
        plugin.saveConfig();
    }
}


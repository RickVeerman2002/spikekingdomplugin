package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onSetCommand;
import static me.rick.spikekingdomplugin.utils.ScoreboardUtil.createScoreboard;

/**
 * This is the class of the command "/setkingdom". It sets a kingdom to a player.
 * the players with that rank.
 * @author RickVeerman
 */
public class SetKingdomCommand implements CommandExecutor {
    private final SpikeKingdomPlugin plugin;

    /**
     * SetKingdomCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public SetKingdomCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if (args.length != 2) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /setkingdom <player> <kingdom>");
            return true;
        }

        String playerName = args[0];
        String newKingdom= args[1];
        FileConfiguration config = plugin.getConfig();

        setsNewKingdom(config, playerName, newKingdom);

        onSetCommand(config, player);

        createScoreboard(player, plugin);

        player.sendMessage(ChatColor.DARK_GREEN + "Set kingdom: " + newKingdom + " for player " + playerName);
        return true;
    }



    /**
     * Sets a kingdom to the player and saves it.
     * @param config | File Configuration
     * @param playerName | Frist Argument of the Command | Player Name
     * @param newKingdom | Second Argument of the Command | Kingdom Name
     */
    private void setsNewKingdom(FileConfiguration config, String playerName, String newKingdom) {
        config.set("players." + playerName + ".kingdom", newKingdom);
        config.set("players." + playerName + ".kingdomRank", "Inwoner");
        plugin.saveConfig();
    }
}

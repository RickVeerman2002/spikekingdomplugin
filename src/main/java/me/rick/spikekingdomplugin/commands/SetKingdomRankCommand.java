package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import me.rick.spikekingdomplugin.utils.ScoreboardUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.AddPermissionUtil.addsPermissionsToOnePlayer;
import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onSetCommand;
import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesKingdomRankPermissionsFromOnePlayer;
import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesPermissionsFromOnePlayer;

/**
 * This is the class of the command "/setkingdomrank". It sets a kingdom rank to a player.
 * @author RickVeerman
 */
public class SetKingdomRankCommand implements CommandExecutor {
    private final SpikeKingdomPlugin plugin;

    /**
     * SetKingdomRankCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public SetKingdomRankCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if(args.length != 2){
            player.sendMessage(ChatColor.DARK_RED + "Usage: /setkingdomrank <player> <rank>");
            return true;
        }
        String playerName = args[0];
        String kingdomRank = args[1];
        FileConfiguration config = plugin.getConfig();

        //If the config contains the given kingdom rank it runs the "removesOldPermissionsAndAddsNewPermissions" function
        //and sends a succeed message
        if (config.contains("kingdomRanks." + kingdomRank)) {
            removesOldPermissionsAndAddsNewPermissions(config, player, playerName, kingdomRank);
            player.sendMessage(ChatColor.DARK_GREEN + "Successfully set kingdom rank of " + playerName + " to " + kingdomRank);
        }

        onSetCommand(config, player);
        ScoreboardUtil.createScoreboard(player, plugin);
        Bukkit.reload();
        return true;
    }

    /**
     * Removes old permissions sets new rank to the player and adds new permissions ands saves it
     * @param config | File Configuration
     * @param player | Player
     * @param playerName | First Argument of the Command | Player Name
     * @param kingdomRank | Second Argument of the Command | Kingdom Rank Name
     */
   private void removesOldPermissionsAndAddsNewPermissions(FileConfiguration config, Player player, String playerName, String kingdomRank) {
       removesKingdomRankPermissionsFromOnePlayer(plugin, config, player);
       config.set("players." + playerName + ".kingdomRank", kingdomRank);
       addsPermissionsToOnePlayer(plugin, config, player);
       plugin.saveConfig();
   }

}



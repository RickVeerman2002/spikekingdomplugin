package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onSetCommand;
import static me.rick.spikekingdomplugin.utils.ScoreboardUtil.createScoreboard;

/**
 * This is the class of the command "/setkingdomrankprefix". It sets a prefix for a kingdom rank.
 * @author RickVeerman
 */
public class SetKingdomRankPrefixCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * SetKingdomRankPrefixCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public SetKingdomRankPrefixCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if (args.length != 2) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /setkingdomrankprefix <rank> <prefix>");
            return true;
        }
        String kingdomRank = args[0];
        String prefix = args[1];
        FileConfiguration config = plugin.getConfig();
        //If the given kingdom rank doesn't exist it sends an error message.
        if (!config.contains("kingdomRanks." + kingdomRank)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom rank does not exist.");
            return true;
        }

        setRankPrefix(config, kingdomRank, prefix);

        String format = ChatColor.translateAlternateColorCodes('&', prefix );

        createScoreboard(player, plugin);
        onSetCommand(config, player);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully set prefix of kingdom rank " + kingdomRank + " to " + format);
        return true;
    }

    /**
     * Sets a prefix for a rank and saves it.
     * @param config | File Configuration
     * @param kingdomRank | First Argument of the Command | Kingdom Rank Name
     * @param prefix | Second Argument of the Command | Prefix
     */
    private void setRankPrefix(FileConfiguration config, String kingdomRank, String prefix) {
        config.set("kingdomRanks." + kingdomRank + ".prefix", prefix);
        plugin.saveConfig();
    }

}

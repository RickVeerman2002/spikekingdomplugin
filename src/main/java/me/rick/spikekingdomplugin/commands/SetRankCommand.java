package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import me.rick.spikekingdomplugin.utils.ScoreboardUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;


import static me.rick.spikekingdomplugin.utils.AddPermissionUtil.addsPermissionsToOnePlayer;
import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onSetCommand;
import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesPermissionsFromOnePlayer;

/**
 * This is the class of the command "/setrank". It sets a rank to a player.
 * @author RickVeerman
 */
public class SetRankCommand implements CommandExecutor {
    private final SpikeKingdomPlugin plugin;

    /**
     * SetRankCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public SetRankCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if(args.length != 2){
            player.sendMessage(ChatColor.DARK_RED + "Usage: /setrank <player> <rank>");
            return true;
        }
        String playerName = args[0];
        String rank = args[1];
        FileConfiguration config = plugin.getConfig();

        //If the config contains the given rank it runs the "removesOldPermissionsAndAddsNewPermissions" function
        //and sends a succeed message
        if (config.contains("ranks." + rank)) {
            removesOldPermissionsAndAddsNewPermissions(config, player, playerName, rank);
            player.sendMessage(ChatColor.DARK_GREEN + "Successfully set rank of " + playerName + " to " + rank);
        }

        onSetCommand(config, player);
        ScoreboardUtil.createScoreboard(player, plugin);
        Bukkit.reload();
        return true;
    }

    /**
     * Removes old permissions sets new rank to the player and adds new permissions ands saves it
     * @param config | File Configuration
     * @param player | Player
     * @param playerName | First Argument of the Command | Player Name
     * @param rank | Second Argument of the Command | Rank Name
     */
   private void removesOldPermissionsAndAddsNewPermissions(FileConfiguration config, Player player, String playerName, String rank) {
       removesPermissionsFromOnePlayer(plugin, config, player);
       config.set("players." + playerName + ".rank", rank);
       addsPermissionsToOnePlayer(plugin, config, player);
       plugin.saveConfig();
   }

}



package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesKingdomRankPermissionsFromAllPlayers;

/**
 * This is the class of the command "/removekingdomrank". It removes a kingdom rank.
 * @author RickVeerman
 */
public class RemoveKingdomRankCommand implements CommandExecutor {
    private final SpikeKingdomPlugin plugin;

    /**
     * RemoveKingdomRank Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public RemoveKingdomRankCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "This command can only be run by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 1 arguments it sends an error message.
        if (args.length < 1) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /removekingdomrank <rank>");
            return true;
        }
        String kingdomRankToRemove = args[0];
        FileConfiguration config = plugin.getConfig();
        //If the config doesn't contain the given kingdom rank it sends an error message.
        if (!config.contains("kingdomRanks." + kingdomRankToRemove)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom rank does not exist.");
            return true;
        }

        removesKingdomRankPermissionsFromAllPlayers(plugin, config);

        removesRank(config, kingdomRankToRemove);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully removed kingdom rank: " + kingdomRankToRemove);
        Bukkit.reload();
        return true;
    }

    /**
     * Removes the rank from the config and saves it.
     * @param config | File Configuration
     * @param kingdomRankToRemove | First Argument of the Command | Kingdom Rank Name
     */
    private void removesRank(FileConfiguration config, String kingdomRankToRemove) {
        for (String player : config.getConfigurationSection("players").getKeys(false)) {
            if (config.getString("players." + player + ".kingdomRank").equals(kingdomRankToRemove)) {
                config.set("players." + player + ".kingdomRank", null);
            }
        }
        config.set("kingdomRanks." + kingdomRankToRemove, null);
        plugin.saveConfig();
    }
}


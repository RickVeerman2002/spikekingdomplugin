package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;

import static me.rick.spikekingdomplugin.utils.AddPermissionUtil.addsPermissionsToAllPlayers;

/**
 * This is the class of the command "/addkingdomrankpermission". It sets a permission to a kingdom rank and adds the permission to all
 * the players with that rank.
 * @author RickVeerman
 */
public class AddKingdomRankPermissionCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * AddKingdomRankPermissionCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public AddKingdomRankPermissionCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if (args.length != 2) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /addkingdomrankpermission <rank> <permission>");
            return true;
        }

        String kingdomRank = args[0];
        String permission = args[1];
        FileConfiguration config = plugin.getConfig();
        //If the given kingdom rank doesn't exist it sends an error message.
        if (!config.contains("kingdomRanks." + kingdomRank)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom rank does not exist.");
            return true;
        }

        //List of permissions from the kingdom rank you put as your first argument
        List<String> permissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");

        //If the permission list doesn't contain the given permission it runs "addPermissionToRank()", sends a succeed message,
        //runs the "onAddPermissionPlayer()" and then reloads the server.
        //If the permission list does contain the given permission it sends an error message.
        if (!permissions.contains(permission)) {
            addsPermissionToRank(kingdomRank, permission, config, permissions);
            sender.sendMessage(ChatColor.DARK_GREEN + "Successfully added permission " + permission + " to kingdom rank " + kingdomRank);
            addsPermissionsToAllPlayers(plugin, config);
            Bukkit.reload();
        } else {
            player.sendMessage(ChatColor.DARK_RED + "That permission is already added to that kingdom rank.");
        }
        return true;
    }

    /**
     * Adds the permission to the permissions list and sets and saves it to the config.
     * @param kingdomRank | First Argument of the Command | Kingdom Rank Name
     * @param permission | Second Argument of the Command | Permission
     * @param config | File Configuration
     * @param permissions | List of all permissions
     */
    private void addsPermissionToRank(String kingdomRank, String permission, FileConfiguration config, List<String> permissions) {
        permissions.add(permission);
        config.set("kingdomRanks." + kingdomRank + ".permissions", permissions);
        plugin.saveConfig();
    }
}
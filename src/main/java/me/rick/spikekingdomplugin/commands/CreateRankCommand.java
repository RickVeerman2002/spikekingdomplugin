package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * This is the class of the command "/createrank". It creates a rank.
 * @author RickVeerman
 */
public class CreateRankCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * CreateRankCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public CreateRankCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "This command can only be run by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 1 arguments it sends an error message.
        if (args.length != 1) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /createrank <rank>");
            return true;
        }
        String rank = args[0];
        FileConfiguration config = plugin.getConfig();
        //If the config contains the given rank it sends an error message.
        if (config.contains("ranks." + rank)) {
            player.sendMessage(ChatColor.DARK_RED + "That rank already exists.");
            return true;
        }

        createsRank(config, rank);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully created rank: " + rank);
        return true;
    }

    /**
     * Adds the rank to the config and saves it.
     * @param config | File Configuration
     * @param rank | First Argument of the Command | Rank Name
     */
    private void createsRank(FileConfiguration config, String rank) {
        config.set("ranks." + rank, "");
        plugin.saveConfig();
    }
}

package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * This is the class of the command "/createkingdomrank". It creates a kingdom rank.
 * @author RickVeerman
 */
public class CreateKingdomRankCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * CreateKingdomRankCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public CreateKingdomRankCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "This command can only be run by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 1 arguments it sends an error message.
        if (args.length != 1) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /createkingdomrank <rank>");
            return true;
        }


        String kingdomRank = args[0];
        FileConfiguration config = plugin.getConfig();
        //If the config contains the given kingdom rank it sends an error message.
        if (config.contains("kingdomRanks." + kingdomRank)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom rank already exists.");
            return true;
        }

        createsRank(config, kingdomRank);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully created kingdom rank: " + kingdomRank);
        return true;
    }

    /**
     * Adds the kingdom rank to the config and saves it.
     * @param config | File Configuration
     * @param kingdomRank | First Argument of the Command | Kingdom Rank Name
     */
    private void createsRank(FileConfiguration config, String kingdomRank) {
        config.set("kingdomRanks." + kingdomRank, "");
        plugin.saveConfig();
    }
}

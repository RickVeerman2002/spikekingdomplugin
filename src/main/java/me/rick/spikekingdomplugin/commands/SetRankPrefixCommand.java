package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onSetCommand;
import static me.rick.spikekingdomplugin.utils.ScoreboardUtil.createScoreboard;

/**
 * This is the class of the command "/setrankprefix". It sets a prefix for a rank.
 * @author RickVeerman
 */
public class SetRankPrefixCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * SetRankPrefixCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public SetRankPrefixCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if (args.length != 2) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /setrankprefix <rank> <prefix>");
            return true;
        }
        String rank = args[0];
        String prefix = args[1];
        FileConfiguration config = plugin.getConfig();
        //If the given rank doesn't exist it sends an error message.
        if (!config.contains("ranks." + rank)) {
            player.sendMessage(ChatColor.DARK_RED + "That rank does not exist.");
            return true;
        }

        setRankPrefix(config, rank, prefix);

        String format = ChatColor.translateAlternateColorCodes('&', prefix );

        createScoreboard(player, plugin);
        onSetCommand(config, player);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully set prefix of rank " + rank + " to " + format);
        return true;
    }

    /**
     * Sets a prefix for a rank and saves it.
     * @param config | File Configuration
     * @param rank | First Argument of the Command | Rank Name
     * @param prefix | Second Argument of the Command | Prefix
     */
    private void setRankPrefix(FileConfiguration config, String rank, String prefix) {
        config.set("ranks." + rank + ".prefix", prefix);
        plugin.saveConfig();
    }

}

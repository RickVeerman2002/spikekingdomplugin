package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onSetCommand;
import static me.rick.spikekingdomplugin.utils.ScoreboardUtil.createScoreboard;

/**
 * This is the class of the command "/setkingdomprefix". It sets a prefix for a kingdom.
 * the players with that rank.
 * @author RickVeerman
 */
public class SetKingdomPrefixCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * SetKingdomPrefixCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public SetKingdomPrefixCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if (args.length != 2) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /setkingdomprefix <kingdom> <prefix>");
            return true;
        }

        String kingdom = args[0];
        String prefix = args[1];
        FileConfiguration config = plugin.getConfig();
        //If the given kingdom doesn't exist it sends an error message.
        if (!config.contains("kingdoms." + kingdom)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom does not exist.");
            return true;
        }

        setsKingdomPrefix(config, kingdom, prefix);

        String format = ChatColor.translateAlternateColorCodes('&', prefix );

        createScoreboard(player, plugin);

        onSetCommand(config, player);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully set prefix of kingdom " + kingdom + " to " + format);
        return true;
    }

    /**
     * Sets a prefix for a kingdom and saves it.
     * @param config | File Configuration
     * @param kingdom | First Argument of the Command | Kingdom Name
     * @param prefix | Second Argument of the Command | Prefix
     */
    private void setsKingdomPrefix(FileConfiguration config, String kingdom, String prefix) {
        config.set("kingdoms." + kingdom + ".prefix", prefix);
        plugin.saveConfig();
    }
}

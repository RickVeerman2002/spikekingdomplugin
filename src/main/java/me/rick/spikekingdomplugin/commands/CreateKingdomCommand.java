package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * This is the class of the command "/createkingdom". It creates a kingdom.
 * @author RickVeerman
 */
public class CreateKingdomCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * CreateKingdomCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public CreateKingdomCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.DARK_RED + "This command can only be run by a player.");
            return true;
        }
        Player player = (Player) sender;

        //If you give not 1 arguments it sends an error message.
        if (args.length != 1) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /createkingdom <kingdom>");
            return true;
        }

        String kingdom = args[0];
        FileConfiguration config = plugin.getConfig();
        //If the config contains the given kingdom it sends an error message.
        if (config.contains("kingdoms." + kingdom)) {
            player.sendMessage(ChatColor.DARK_RED + "That kingdom already exists.");
            return true;
        }

        createsKingdom(config, kingdom);

        player.sendMessage(ChatColor.DARK_GREEN + "Successfully created kingdom: " + kingdom);
        return true;
    }

    /**
     * Adds the kingdom to the config and saves it.
     * @param config| File Configuration
     * @param kingdom | First Argument of the Command | Kingdom Name
     */
    private void createsKingdom(FileConfiguration config, String kingdom) {
        config.set("kingdoms." + kingdom, "");
        plugin.saveConfig();
    }
}

package me.rick.spikekingdomplugin.commands;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import java.util.List;

import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesKingdomRankPermissionFromAllPlayer;
import static me.rick.spikekingdomplugin.utils.RemovePermissionUtil.removesPermissionFromAllPlayer;

/**
 * This is the class of the command "/removekingdomrankpermission". It removes a permission from a kingdom rank and removes the permission to from
 * all the players with that rank.
 * @author RickVeerman
 */
public class RemoveKingdomRankPermissionCommand implements CommandExecutor {

    private final SpikeKingdomPlugin plugin;

    /**
     * RemoveKingdomRankPermissionCommand Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public RemoveKingdomRankPermissionCommand(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        //If sender is not a player you get an error message.
        if (!(sender instanceof Player)) {
            sender.sendMessage("This command can only be executed by a player.");
            return true;
        }

        Player player = (Player) sender;

        //If you give not 2 arguments it sends an error message.
        if (args.length != 2) {
            player.sendMessage(ChatColor.DARK_RED + "Usage: /removekingdomrankpermission <rank> <permission>");
            return true;
        }
        String kingdomRank = args[0];
        String permission = args[1];
        FileConfiguration config = plugin.getConfig();

        //If the given rank doesn't exist it sends an error message.
        if (!config.contains("kingdomRanks." + kingdomRank)) {
            player.sendMessage(ChatColor.DARK_RED + "That rank does not exist.");
            return true;
        }

        //If rank given doesn't have any permissions  it sends an error message.
        if (!config.contains("kingdomRanks." + kingdomRank + ".permissions")) {
            player.sendMessage(ChatColor.DARK_RED + "That rank does not have any permissions");
            return true;
        }

        //If rank given doesn't have the permission you gave it sends an error message.
        if(!config.getStringList("kingdomRanks." + kingdomRank + ".permissions").contains(permission)) {
            player.sendMessage(ChatColor.DARK_RED + "That permission is not assigned to that kingdom rank.");
            return true;
        }

        //List of permissions from the kingdom rank you put as your first argument
        List<String> permissions = config.getStringList("kingdomRanks." + kingdomRank + ".permissions");

        removesKingdomRankPermissionFromAllPlayer(plugin, config, permission);

        removesPermission(config, permissions, permission, kingdomRank);


        player.sendMessage(ChatColor.DARK_GREEN + "Successfully removed permission " + permission + " from kingdom rank " + kingdomRank);

        Bukkit.reload();
        return true;
    }

    /**
     * Adds the permission to the permissions list and sets and saves it to the config.
     * @param config | File Configuration
     * @param permissions | List of all permissions
     * @param permission | Second Argument of the Command | Permission
     * @param kingdomRank First Argument of the Command | Kingdom Rank Name
     */
    private void removesPermission(FileConfiguration config, List<String> permissions, String permission, String kingdomRank) {
        permissions.remove(permission);
        config.set("kingdomRanks." + kingdomRank + ".permissions", permissions);
        plugin.saveConfig();
    }

}



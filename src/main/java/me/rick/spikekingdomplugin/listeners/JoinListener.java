package me.rick.spikekingdomplugin.listeners;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import me.rick.spikekingdomplugin.utils.ScoreboardUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static me.rick.spikekingdomplugin.utils.AddPermissionUtil.addsPermissionsToOnePlayer;
import static me.rick.spikekingdomplugin.utils.ChatPrefixUtil.onJoinListener;

/**
 * This class is used when a player joins.
 * @author RickVeerman
 */
public class JoinListener implements Listener {

    private final SpikeKingdomPlugin plugin;

    /**
     * JoinListener Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public JoinListener(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();
        FileConfiguration config = plugin.getConfig();

        //If a player joins the server sees a nice join message
        String joinMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("join-message"));
        String newJoinMessage = joinMessage.replaceAll("%PLAYER%", player.getDisplayName());

        event.setJoinMessage(newJoinMessage);

        onJoinListener(plugin,config, player);

        addsPermissionsToOnePlayer(plugin, config, player);

        ScoreboardUtil.createScoreboard(player, plugin);

        Bukkit.reload();
    }
}

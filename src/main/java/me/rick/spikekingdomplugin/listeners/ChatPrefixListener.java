package me.rick.spikekingdomplugin.listeners;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.configuration.Configuration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * This class is used when a player types something in the chat
 * @author RickVeerman
 */
public class ChatPrefixListener implements Listener {

    private final SpikeKingdomPlugin plugin;

    /**
     * ChatPrefixListener Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public ChatPrefixListener(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void PlayerChatPrefix(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        FileConfiguration config = plugin.getConfig();

        String rank;
        String kingdom;
        String kingdomRank;
        String rankPrefix;
        String kingdomPrefix;
        String kingdomRankPrefix;


        //If the player that sends a message, and he has a rank. Rank gets a value
        if (config.contains("players." + player.getName() + ".rank")) {
            rank = config.getString("players." + player.getName() + ".rank");
        } else {
            rank = null;
        }

        //If the player that sends a message is part of a kingdom. Kingdom gets a value
        if(config.contains("players." + player.getName() + ".kingdom")) {
            kingdom = config.getString("players." + player.getName() + ".kingdom");
        } else {
            kingdom = null;
        }

        //If the player that sends a message, and he has a kingdom rank. Kingdom Rank gets a value
        if(config.contains("players." + player.getName() + ".kingdomRank")) {
            kingdomRank = config.getString("players." + player.getName() + ".kingdomRank");
        } else {
            kingdomRank = null;
        }

        //Iff rank/kingdom has a prefix rankPrefix/kingdomPrefix gets a value
        rankPrefix = config.getString("ranks." + rank + ".prefix");
        kingdomRankPrefix = config.getString("kingdomRanks." + kingdomRank + ".prefix");
        kingdomPrefix = config.getString("kingdoms." + kingdom + ".prefix");
        //If rankPrefix is not null, kingdomPrefix is null and kingdomRankPrefix is null the player gets a rank prefix
        if (rankPrefix != null && kingdomPrefix == null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + rankPrefix + "&7] %1$s&f: %2$s" );
            event.setFormat(format);
        }
        //If kingdomPrefix & rankPrefix are not null the player gets a rank and kingdom prefix
        if (kingdomPrefix != null && rankPrefix != null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + rankPrefix + "&7][" + kingdomPrefix + "&7] %1$s&f: %2$s" );
            event.setFormat(format);
        }

        //If kingdomPrefix & rankPrefix are not null the player gets a rank and kingdom prefix
        if (kingdomPrefix != null && rankPrefix != null && kingdomRankPrefix != null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + rankPrefix + "&7][" + kingdomPrefix + "&7][" + kingdomRankPrefix + "&7] %1$s&f: %2$s" );
            event.setFormat(format);
        }

        //If rankPrefix is null and kingdomPrefix is not null the player gets a kingdom prefix
        if (rankPrefix == null && kingdomPrefix != null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + kingdomPrefix + "&7] %1$s&f: %2$s" );
            event.setFormat(format);
        }

        //If rankPrefix is null and kingdomPrefix is not null the player gets a kingdom prefix
        if (rankPrefix == null && kingdomPrefix != null && kingdomRankPrefix != null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7[" + kingdomPrefix + "&7][" + kingdomRankPrefix + "&7] %1$s&f: %2$s" );
            event.setFormat(format);
        }

        //If kingdomPrefix & rankPrefix are null the player gets no prefix
        if (rankPrefix == null && kingdomPrefix == null && kingdomRankPrefix == null) {
            String format = ChatColor.translateAlternateColorCodes('&', "&7%1$s&f: %2$s" );
            event.setFormat(format);
        }
    }
}


package me.rick.spikekingdomplugin.listeners;

import me.rick.spikekingdomplugin.SpikeKingdomPlugin;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * This class is used when a player quits.
 * @author RickVeerman
 */
public class QuitListener implements Listener {
    private final SpikeKingdomPlugin plugin;

    /**
     * QuitsListener Constructor
     * @param plugin | SpikeKingdomPlugin
     */
    public QuitListener(SpikeKingdomPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        //If a player quits the server sees a nice quit message
        String quitMessage = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("quit-message"));
        String newQuitMessage = quitMessage.replaceAll("%PLAYER%", player.getDisplayName());

        event.setQuitMessage(newQuitMessage);
    }
}

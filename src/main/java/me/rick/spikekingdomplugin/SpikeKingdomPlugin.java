package me.rick.spikekingdomplugin;

import me.rick.spikekingdomplugin.commands.*;
import me.rick.spikekingdomplugin.listeners.ChatPrefixListener;
import me.rick.spikekingdomplugin.listeners.JoinListener;
import me.rick.spikekingdomplugin.listeners.QuitListener;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * This class creates the commands and registers the Listeners.
 * @author RickVeerman
 */
public class SpikeKingdomPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();

        getCommand("createkingdom").setExecutor(new CreateKingdomCommand(this));
        getCommand("setrank").setExecutor(new SetRankCommand(this));
        getCommand("setrankprefix").setExecutor(new SetRankPrefixCommand(this));
        getCommand("setkingdom").setExecutor(new SetKingdomCommand(this));
        getCommand("setkingdomprefix").setExecutor(new SetKingdomPrefixCommand(this));
        getCommand("createrank").setExecutor(new CreateRankCommand(this));
        getCommand("removerank").setExecutor(new RemoveRankCommand(this));
        getCommand("removekingdom").setExecutor(new RemoveKingdomCommand(this));
        getCommand("addrankpermission").setExecutor(new AddRankPermissionCommand(this));
        getCommand("removerankpermission").setExecutor(new RemoveRankPermissionCommand(this));
        getCommand("addkingdomrankpermission").setExecutor(new AddKingdomRankPermissionCommand(this));
        getCommand("createkingdomrank").setExecutor(new CreateKingdomRankCommand(this));
        getCommand("removekingdomrank").setExecutor(new RemoveKingdomRankCommand(this));
        getCommand("removekingdomrankpermission").setExecutor(new RemoveKingdomRankPermissionCommand(this));
        getCommand("setkingdomrank").setExecutor(new SetKingdomRankCommand(this));
        getCommand("setkingdomrankprefix").setExecutor(new SetKingdomRankPrefixCommand(this));


        getServer().getPluginManager().registerEvents(new JoinListener(this), this);
        getServer().getPluginManager().registerEvents(new QuitListener(this), this);
        getServer().getPluginManager().registerEvents(new ChatPrefixListener(this), this);
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
